window.onclick = function(event) {
  var dropdown = document.getElementById('dropdown');
  
  if (dropdown.contains(event.target)){
    dropdown.classList.toggle('dropdown--open');
  } else {
    dropdown.classList.remove('dropdown--open');
  }
}
